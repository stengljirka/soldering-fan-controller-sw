# Soldering Fan Controller SW

## Description
Software part of project to control two fans to soak soldering fumes.
Both fans can be turned on/off by buttons and their speed it set by potentiometer. Project is based on Platformio framework.

## Hardware part
Link to repository with hardware for project: https://gitlab.com/stengljirka/soldering-fan-controller

## License
Project is under GPLv3 licence. 
