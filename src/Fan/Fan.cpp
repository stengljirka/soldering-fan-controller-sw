//
// Created by jiri on 6/4/22.
//

#include "Fan.h"
#include <Arduino.h>
#include "assert.h"

#define MAX_PWM_VALUE 255.0
#define TRUNCATE_CONST 0.5


bool Fan::frequencyInited = false;
void Fan::initFrequency() {
    if (frequencyInited) {
        return;
    }
    frequencyInited = true;
    TCCR1B = (TCCR1B & B11111000) | B00000001;
}


Fan::Fan(uint8_t fanOutputPin, float power, bool isOn) : fanOutputPin(fanOutputPin), power(power), isOn(isOn) {
    pinMode(fanOutputPin, OUTPUT);
    initFrequency();
    if (isOn) {
        analogWrite(fanOutputPin, powerInPWM());
    } else {
        analogWrite(fanOutputPin, 0);
    }
}
/*
 * Calculates power from potentiometer into percentage
 */
uint8_t Fan::powerInPWM() {
    if (Fan::power > MAX_FAN_POWER) {
        assert(false);
    }
    double newPower = (MAX_PWM_VALUE / MAX_FAN_POWER) * power + TRUNCATE_CONST;
    return (uint8_t) newPower;
}

float Fan::getPower() const {
    return power;
}

void Fan::setPower(float newPower) {
    power = newPower;
    if (isOn){
        analogWrite(fanOutputPin, powerInPWM());
    }else{
        analogWrite(fanOutputPin, 0);
    }
}

void Fan::setRunning(bool status) {
    isOn = status;
    if (status){
        analogWrite(fanOutputPin, powerInPWM());
    }else{
        analogWrite(fanOutputPin, 0);
    }

}

bool Fan::isFanOn() const {
    return isOn;
}









