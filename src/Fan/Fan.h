//
// Created by jiri on 6/4/22.
//

#ifndef FAN_CONTROLLER_FAN_H
#define FAN_CONTROLLER_FAN_H
#include <stdint-gcc.h>

#define MAX_FAN_POWER 100.0


class Fan {
private:
    const uint8_t fanOutputPin;
    float power; // in percentage
    bool isOn;
public:
    float getPower() const;
    void setPower(float newPower);
    void setRunning(bool status);
    Fan(uint8_t fanOutputPin, float power, bool isOn);
private:
    static void initFrequency();

public:
    bool isFanOn() const;

private:
    static bool frequencyInited;
    uint8_t powerInPWM();

};


#endif //FAN_CONTROLLER_FAN_H
