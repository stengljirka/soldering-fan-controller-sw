//
// Created by jiri on 6/5/22.
//

#include "Input.h"
#include <Arduino.h>

#define MAX_VALUE 1023.0f
#define PERCENTAGE 100.0f
EepromHandler& Input::eepromHandler = EepromHandler::getInstance();

Input::Input(const uint8_t buttonPin,
             const uint8_t potentiometerPin,
             Fan &fan,
             const bool isRight) : buttonPin(buttonPin),
                                   potentiometerPin(potentiometerPin),
                                   fan(fan),
                                   isRight(isRight) {
    pinMode(potentiometerPin, INPUT);
    pinMode(buttonPin, INPUT_PULLUP);
    lastPotentiometerValue = UINT16_MAX;
    buttonDebounce = false;
    refreshPowerReading();
}

void Input::refreshPowerReading() {
    uint16_t value = analogRead(potentiometerPin);
    if (value == lastPotentiometerValue) {
        return;
    }
    lastPotentiometerValue = value;
    float newPower = PERCENTAGE - (float) lastPotentiometerValue / MAX_VALUE * PERCENTAGE; //reverse due to the position of potentiometer
    fan.setPower(newPower);
}
/*
 * Button handling with decoupling caused by the waves after clicking + only first click will be saved
 */
void Input::handleButtonReading() {
    if (digitalRead(buttonPin) == LOW) {
        if (!buttonDebounce) {
            buttonDebounce = true;
            if (isRight) {
                bool newStatus = !eepromHandler.isRightFanOn();
                eepromHandler.updateRightFanStatus(newStatus);
                fan.setRunning(newStatus);
            } else {
                bool newStatus = !eepromHandler.isLeftFanOn();
                eepromHandler.updateLeftFanStatus(newStatus);
                fan.setRunning(newStatus);
            }
        }
    } else {
        buttonDebounce = false;
    }
}

void Input::refreshInput() {
    refreshPowerReading();
    handleButtonReading();
}

