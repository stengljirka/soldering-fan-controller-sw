//
// Created by jiri on 6/5/22.
//

#ifndef FAN_CONTROLLER_INPUT_H
#define FAN_CONTROLLER_INPUT_H

#include <stdint-gcc.h>
#include "../Fan/Fan.h"
#include "../EEPROM/EepromHandler.h"


class Input {
private:
    const uint8_t buttonPin;
    const uint8_t potentiometerPin;
    Fan& fan;
    const bool isRight;
    uint16_t lastPotentiometerValue;
    bool buttonDebounce;
    static EepromHandler& eepromHandler;
public:
    Input(uint8_t buttonPin, uint8_t potentiometerPin, Fan &fan, bool isRight);
    void refreshInput();
private:
    void handleButtonReading();
    void refreshPowerReading();

};


#endif //FAN_CONTROLLER_INPUT_H
