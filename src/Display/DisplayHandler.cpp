//
// Created by jiri on 6/6/22.
//

#include "DisplayHandler.h"
#include <Adafruit_SSD1306.h>
#include <Adafruit_GFX.h>
#include <Arduino.h>
#include <stdint-gcc.h>

#define SCREEN_WIDTH 128
#define SCREEN_HEIGHT 64
#define OLED_RESET -1 // Doesn't have reset pin on display
#define DISPLAY_ADDRESS 0x3C

#define TITLE_RIGHT_FAN "RIGHT"
#define TITLE_LEFT_FAN "LEFT"
#define LEFT_X_VERTICAL_SPLIT_LINE 63
#define RIGHT_X_VERTICAL_SPLIT_LINE 64
#define TRUNCATE_CONSTANT 10.0f
#define MIN_NUMBER_LENGTH 2
#define HELP_CHARS_LENGTH 3
#define DECIMAL_POINT '.'
#define PERCENTAGE '%'
#define FAN_ON_TEXT "ON"
#define FAN_OFF_TEXT "OFF"


enum TextSizes {
    NORMAL = 1,
    BIG = 2
};

/*
 * Important to determine where will be placed texts on the screen
 */
enum YCoordinates {
    TITLE_LINE_Y = 15,
    STATUS_Y_START = 16,
    STATUS_Y_END = 40,
    POWER_Y_START = 40,
    POWER_Y_END = SCREEN_HEIGHT
};


static Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

DisplayHandler::DisplayHandler() {
    display.begin(SSD1306_SWITCHCAPVCC, DISPLAY_ADDRESS);
    display.clearDisplay();
    display.display();
}

void DisplayHandler::prepareDisplayHeader() {
    display.clearDisplay();
    display.setTextColor(WHITE);
    display.setTextSize(NORMAL);
    printTextInMiddle(TITLE_LEFT_FAN, 0, SCREEN_WIDTH / 2, 0, TITLE_LINE_Y);
    printTextInMiddle(TITLE_RIGHT_FAN, SCREEN_WIDTH / 2, SCREEN_WIDTH, 0, TITLE_LINE_Y);
    display.drawLine(0, TITLE_LINE_Y, SCREEN_WIDTH, TITLE_LINE_Y, WHITE);
    display.drawLine(LEFT_X_VERTICAL_SPLIT_LINE, 0, LEFT_X_VERTICAL_SPLIT_LINE, SCREEN_HEIGHT, WHITE);
    display.drawLine(RIGHT_X_VERTICAL_SPLIT_LINE, 0, RIGHT_X_VERTICAL_SPLIT_LINE, SCREEN_HEIGHT, WHITE);
}

/*
 * Function for centering the text in the box passed as arguments
 */

void DisplayHandler::printTextInMiddle(const char *text, uint8_t startX, uint8_t endX, uint8_t startY, uint8_t endY) {
    TextDimensions dimensions = getTextDimensions(text);
    uint8_t xOffset = (abs(endX - startX) - dimensions.width) / 2;
    uint8_t yOffset = (abs(endY - startY) - dimensions.height) / 2;
    uint8_t x = startX + xOffset;
    uint8_t y = startY + yOffset;
    display.setCursor(x, y);
    display.print(text);
}


DisplayHandler::TextDimensions DisplayHandler::getTextDimensions(const char *text) {
    int16_t x1, y1;
    TextDimensions dimensions = {
            .width = 0,
            .height = 0
    };
    display.getTextBounds(text, 0, 0, &x1, &y1, &dimensions.width, &dimensions.height);
    return dimensions;
}

/*
 * Extracts length of number to be displayed on display
 */
uint8_t DisplayHandler::getNumberOfNumbers(uint16_t number) {
    if (number == 0) {
        return MIN_NUMBER_LENGTH;
    }
    uint8_t counter;
    for (counter = 0; number != 0; ++counter) {
        number /= 10;
    }
    return counter;
}


void DisplayHandler::printPower(float power, uint8_t startX, uint8_t endX, uint8_t startY, uint8_t endY) {
    display.setTextSize(NORMAL);
    uint16_t truncatedPower = (uint16_t) (power * TRUNCATE_CONSTANT);
    uint8_t length = getNumberOfNumbers(truncatedPower);
    char text[length + HELP_CHARS_LENGTH];
    fillTextWithPowerInCorrectFormat(text, power, length);
    printTextInMiddle(text, startX, endX, startY, endY);


}

/*
 * Needed to fill text buffer with correct power value and decimal point + percentage
 */
void DisplayHandler::fillTextWithPowerInCorrectFormat(char *text, float power, uint8_t length) {
    uint16_t powerTruncated = (uint16_t) (power * TRUNCATE_CONSTANT);
    char digits[length];
    for (int i = length - 1; i >= 0; --i) {
        digits[i] = (char) ('0' + powerTruncated % 10);
        powerTruncated /= 10;
    }
    for (uint8_t i = 0; i < length - 1; ++i) {
        text[i] = digits[i];
    }
    text[length - 1] = DECIMAL_POINT;
    text[length] = digits[length - 1];
    text[length + 1] = PERCENTAGE;
    text[length + 2] = '\0';
}

void DisplayHandler::printFanStatus(bool status, uint8_t startX, uint8_t endX, uint8_t startY, uint8_t endY) {
    display.setTextSize(BIG);
    if (status) {
        printTextInMiddle(FAN_ON_TEXT, startX, endX, startY, endY);
    } else {
        printTextInMiddle(FAN_OFF_TEXT, startX, endX, startY, endY);
    }
}


void DisplayHandler::update(const Fan &rightFan, const Fan &leftFan) {
    prepareDisplayHeader();
    printFanStatus(rightFan.isFanOn(), SCREEN_WIDTH / 2, SCREEN_WIDTH, STATUS_Y_START, STATUS_Y_END);
    printFanStatus(leftFan.isFanOn(), 0, SCREEN_WIDTH / 2, STATUS_Y_START, STATUS_Y_END);
    printPower(rightFan.getPower(), SCREEN_WIDTH / 2, SCREEN_WIDTH, POWER_Y_START, POWER_Y_END);
    printPower(leftFan.getPower(), 0, SCREEN_WIDTH / 2, POWER_Y_START, POWER_Y_END);
    display.display();

}

DisplayHandler &DisplayHandler::getInstance() {
    static DisplayHandler displayHandler;
    return displayHandler;
}