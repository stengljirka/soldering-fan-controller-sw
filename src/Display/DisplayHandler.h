//
// Created by jiri on 6/6/22.
//

#ifndef UNTITLED_DISPLAY_HANDLER_H
#define UNTITLED_DISPLAY_HANDLER_H


#include "Fan/Fan.h"

class DisplayHandler {
    struct TextDimensions {
        uint16_t width;
        uint16_t height;
    };

private:
    DisplayHandler();

    void prepareDisplayHeader();

    void printTextInMiddle(const char *text, uint8_t startX, uint8_t endX, uint8_t startY, uint8_t endY);

    void printPower(float power, uint8_t startX, uint8_t endX, uint8_t startY, uint8_t endY);

    uint8_t getNumberOfNumbers(uint16_t number);

    void fillTextWithPowerInCorrectFormat(char *text, float power, uint8_t length);

    void printFanStatus(bool status, uint8_t startX, uint8_t endX, uint8_t startY, uint8_t endY);

    TextDimensions getTextDimensions(const char *text);


public:
    DisplayHandler(DisplayHandler const &) = delete;

    void operator=(DisplayHandler const &) = delete;

    static DisplayHandler &getInstance();

    void update(const Fan &rightFan, const Fan &leftFan);
};


#endif //UNTITLED_DISPLAY_HANDLER_H
