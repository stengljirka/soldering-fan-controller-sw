//
// Created by jiri on 6/4/22.
//

#include "EepromHandler.h"
#include "EEPROM.h"

#define EEPROM_MAGIC_CONSTANT 69
#define EEPROM_MAGIC_CONSTANT_INDEX 0
#define EEPROM_SIZE 512

enum {
    LEFT_FAN_INDEX = 1,
    RIGHT_FAN_INDEX = 2
};

enum {
    ON = 1,
    OFF = 0
};


EepromHandler::EepromHandler() {
    if (EEPROM.read(EEPROM_MAGIC_CONSTANT_INDEX) != EEPROM_MAGIC_CONSTANT) {
        for (int i = 0; i < EEPROM_SIZE; ++i) {
            EEPROM.write(i, 0);
        }
        EEPROM.write(EEPROM_MAGIC_CONSTANT_INDEX, EEPROM_MAGIC_CONSTANT);
    }
    EepromHandler::leftFanOn = EEPROM.read(LEFT_FAN_INDEX) == ON;
    EepromHandler::rightFanOn = EEPROM.read(RIGHT_FAN_INDEX) == ON;
}

bool EepromHandler::isRightFanOn() {
    return rightFanOn;
}

bool EepromHandler::isLeftFanOn() {
    return leftFanOn;
}

void EepromHandler::updateLeftFanStatus(bool status) {
    if (status != leftFanOn) {
        leftFanOn = status;
        int valueToWrite = status ? ON : OFF;
        EEPROM.write(LEFT_FAN_INDEX, valueToWrite);
    }
}

void EepromHandler::updateRightFanStatus(bool status) {
    if (status != rightFanOn) {
        rightFanOn = status;
        int valueToWrite = status ? ON : OFF;
        EEPROM.write(RIGHT_FAN_INDEX, valueToWrite);
    }
}

EepromHandler &EepromHandler::getInstance() {
    static EepromHandler instance;
    return instance;
}
