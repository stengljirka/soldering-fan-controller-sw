//
// Created by jiri on 6/4/22.
//

#ifndef FAN_CONTROLLER_EEPROM_HANDLER_H
#define FAN_CONTROLLER_EEPROM_HANDLER_H


class EepromHandler {
private:
    EepromHandler();
    bool leftFanOn;
    bool rightFanOn;
public:
    EepromHandler(EepromHandler const&) = delete;
    void operator=(EepromHandler const&) = delete;
    static EepromHandler& getInstance();
    bool isRightFanOn();
    bool isLeftFanOn();
    void updateRightFanStatus(bool status);
    void updateLeftFanStatus(bool status);

};


#endif //FAN_CONTROLLER_EEPROM_HANDLER_H
