#include <Arduino.h>
#include "Fan/Fan.h"
#include "EEPROM/EepromHandler.h"
#include "Input/Input.h"
#include "Display/DisplayHandler.h"

#define RIGHT_FAN_PIN 9
#define LEFT_FAN_PIN 10


Fan rightFan(RIGHT_FAN_PIN, 0, false);
Fan leftFan(LEFT_FAN_PIN, 0, false);
Input rightFanInput(PD2, PC0, rightFan, true);
Input leftFanInput(PD3, PC1, leftFan, false);


void setup() {
    EepromHandler &handler = EepromHandler::getInstance();
    rightFan.setRunning(handler.isRightFanOn());
    leftFan.setRunning(handler.isLeftFanOn());
}

void loop() {
    rightFanInput.refreshInput();
    leftFanInput.refreshInput();
    DisplayHandler::getInstance().update(rightFan, leftFan);
}